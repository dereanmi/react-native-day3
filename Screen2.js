import React, { Component } from 'react'
import { View, Text, Button } from 'react-native'
import { Link } from 'react-router-native'

class Screen2 extends Component {

    goToScreen1 = () => {
        this.props.history.push('/screen1', { //เก็บประวัติว่าเคยเข้าหน้าscreen1ไปแล้ว(stack),เมื่อกดปุ่มButtonจะpushกลับไปหน้าscreen1
           mymessage: 'You return to screen 1' //ในconsoleจะreturn {mynumber 20}
        }) 
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#AED6F1' }}>
                <Text style={{ color: 'white', textAlign: 'center', fontSize: 30 }}>Screen 2</Text>
                <Link to="/screen1">
                    <Text style={{ color: 'white', textAlign: 'center', fontSize: 20 }}>Back to Screen 1</Text>
                </Link>
                <Button title="Back to Screen 1" onPress={this.goToScreen1} />
            </View>
        )
    }
}
export default Screen2