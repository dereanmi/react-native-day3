import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, ScrollView, Image, Button, Alert, TouchableOpacity } from 'react-native';
import { Link } from 'react-router-native'



class Login extends React.Component {

    state = {
        username: '',
        password: ''
    };

// onClickLogin() {
//     this.setState()
// }

goToScreen1 = () => {
    this.props.history.push('/profile', { 
       myusername:  this.state.username
    }) 
}


    render() {
        return (

            <View style={styles.container} >
                <View style={styles.content}>
                    <View style={[styles.layout1, styles.center]}>
                        <Text style={[styles.center, styles.headerStyle]}>
                            Login Page
                    </Text>
                        <View style={[styles.logo, styles.center]}>
                            <Image source={require('./profile2.jpg')} style={[styles.logo]} />
                        </View>
                    </View>

                    <Text> {this.state.username}-{this.state.password} </Text>
                    <View style={[styles.layout2, styles.center]}>
                        <View style={[styles.textInput, styles.center, styles.radius]}>
                            <TextInput
                                style={styles.style}
                                onChangeText={(username) => this.setState({ username })}>
                                Username
                            </TextInput>
                        </View>

                        <View style={[styles.textInput, styles.center, styles.radius]}>
                            <TextInput
                                style={styles.style}
                                onChangeText={(password) => this.setState({ password })}>
                                Password
                            </TextInput>
                        </View>

                        <View style={[styles.touchable, styles.radius, styles.center]}>

                            <TouchableOpacity onPress={this.goToScreen1}>
                                <Text style={styles.textLogin}>Login</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({

    container: {
        backgroundColor: '#212F3C',
        flex: 1
    },

    content: {
        backgroundColor: '#212F3C',
        flex: 1,
        flexDirection: 'column'
    },

    layout1: {
        backgroundColor: '#212F3C',
        flex: 1,
        flexDirection: 'column'
    },

    layout2: {
        backgroundColor: '#212F3C',
        flex: 1,
        flexDirection: 'column'
    },

    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    logo: {
        borderRadius: 100,
        width: 200,
        height: 200
    },

    image: {
        backgroundColor: '#7DCEA0',
        margin: 50,
    },

    textInput: {
        backgroundColor: '#505392',
        flex: 1,
        padding: 12,
        margin: 2
    },

    textLogin: {
        color: 'white',
        textAlign: 'center',
        fontSize: 20
    },

    touchable: {
        backgroundColor: '#263A8A',
        flex: 1,
        margin: 70,
    },
    style: {
        color: 'white',
        textAlign: 'center',
        fontSize: 20,
    },
    headerStyle: {
        color: 'white',
        textAlign: 'center',
        fontSize: 27,
        margin: 20
    },
    radius: {
        borderRadius: 100,
        width: 400,
        height: 400
    }


})
export default Login
