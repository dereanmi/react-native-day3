//ไฟล์สำหรับเปลี่ยนหน้า โดยใช้tag route โดยจะประกอบด้วย3tier ลึกลงไป ได้แก่ nativerouter,switch,route
import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import Screen1 from './Screen1'
import Screen2 from './Screen2'
import Login from './Login'
import Profile from './Profile'


class Router extends Component {
    render() {
        return (
            <NativeRouter>
                <Switch>
                    <Route exact path="/login" component={Login} />
                    <Route exact path="/profile" component={Profile} />
                    <Route exact path="/screen1" component={Screen1} />
                    <Route exact path="/screen2" component={Screen2} />
                    <Redirect to="/login" />
                </Switch>
            </NativeRouter>
        )
    }
}
export default Router